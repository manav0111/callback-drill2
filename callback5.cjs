// Problem 5: Write a function that will use the previously written functions to get the following information. You do not need to pass control back to the code that called it.

// Get information from the Thanos boards
// Get all the lists for the Thanos board
// Get all cards for the Mind and Space lists simultaneously

const callback1 = require("./callback1.cjs");
const callback2 = require("./callback2.cjs");
const callback3 = require("./callback3.cjs");

const callback5 = (name, Boardsdata, Listdata, Carddata) => {
  let id;

  Boardsdata.forEach((Object) => {
    if (Object.name == name) {
      id = Object.id;
    }
  });

  console.log(id);

  let cb = (error, msg) => {
    if (error) {
      console.log(error);
    } else {
      console.log(msg);
    }
  };

  callback1(id, Boardsdata, cb);

  cb = (error, msg) => {
    if (error) {
      console.log(error);
    } else {
      let IDArray = [];

      msg.forEach((obj) => {
        if (obj.name == "Mind" || obj.name == "Space") {
          console.log(obj.id, obj.name);
          IDArray.push(obj.id);
        }
        console.log(`Id - ${obj.id}, Name - ${obj.name} `);
      });

      cb = (error, msg) => {
        if (error) {
          console.log(error);
        } else {
          msg.forEach((obj) => {
            console.log(`Id - ${obj.id}, Description - ${obj.description} `);
          });
        }
      };

      callback3(IDArray[0], Carddata, cb);
      callback3(IDArray[1], Carddata, cb);
    }
  };

  callback2(id, Listdata, cb);
};

module.exports = callback5;
