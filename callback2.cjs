// Problem 2: Write a function that will return all lists that belong to a board based on the boardID that is passed to it from the given data in lists.json. Then pass control back to the code that called it by using a callback function.


const callback2 = (id, Listdata, cb) => {
    setTimeout(() => {
        const ListInfo=Listdata[id];
        
        if(ListInfo.length)
        {
            cb(null,ListInfo);

        }
        else
        {
            cb("List id is Not Match",null);
        }
    
    }, 2 * 1000);
  };
  

  module.exports = callback2;
  